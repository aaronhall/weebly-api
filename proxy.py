#!/usr/bin/env python

from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.serving import run_simple

from frontend.app import application as frontend
from api.app import application as api
from api.config import API_PATH_PREFIX


"""
The DispatcherMiddleware is a simple way to combine multiple WSGI applications in the development
environment. The frontend (Flask) application will serve all requests by default. Request paths
prefixed with '/api' will be routed to the api (Falcon) application.

In a product setting, each of these applications would be served by their own WSGI server instance
(using gunicorn or similar).
"""

application = DispatcherMiddleware(frontend, {
    API_PATH_PREFIX: api
})


if __name__ == '__main__':
    run_simple('localhost', 5000, application, use_reloader=True, use_debugger=True, use_evalex=False)