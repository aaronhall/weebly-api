FROM python:2.7-wheezy

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential python-dev libmysqlclient-dev

ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["./run"]