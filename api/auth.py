from falcon_auth import FalconAuthMiddleware, TokenAuthBackend
from api.db import get_db_session


def user_loader(token):
    return { 'username': 'foo' }

auth_backend = TokenAuthBackend(user_loader)
auth_middleware = FalconAuthMiddleware(auth_backend, exempt_methods=['HEAD'])

default_resource_auth = {
    'backend': TokenAuthBackend(user_loader=user_loader),
}