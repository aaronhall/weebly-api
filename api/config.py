

API_PATH_PREFIX = '/api/v1'
DATABASE_PREFIX = 'mysql://root:password@mysql'

DATABASES = {
    'prod': '%s/weebly_api' % DATABASE_PREFIX,
    'test': '%s/weebly_api_test3' % DATABASE_PREFIX,
}