import falcon
from falcon_auth import FalconAuthMiddleware, TokenAuthBackend

from api.db import get_engine, get_session
from api.middleware.JsonMiddleware import JsonMiddleware
from api.models.User import User
from api.resources import *


def create_app(test_app=False):
    # Initialize database connection
    db_key = 'test' if test_app else 'prod'
    engine = get_engine(db_key)

    # Authentication middleware
    def user_loader(token):
        db = get_session(db_key)
        user = db.query(User).filter(User.api_token == token).first()
        db.close()
        return user if user else None

    auth_backend = TokenAuthBackend(user_loader)
    auth_middleware = FalconAuthMiddleware(auth_backend)

    # Initialize Falcon WSGI app
    app = falcon.API(middleware=[
        auth_middleware,
        JsonMiddleware(),
    ])
    app.add_route('/sites', SiteCollection(engine))
    app.add_route('/sites/{site_id}', SiteResource(engine))

    return app


api = application = create_app()
