from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship
from Base import Base
from Element import Element


class Page(Base):
    __tablename__ = 'pages'
    __table_args__ = (UniqueConstraint('site_id', 'order'),)

    id = Column(Integer, primary_key=True)
    site_id = Column(Integer, ForeignKey('sites.id'), nullable=False)

    name = Column(String(255), nullable=False)
    url_slug = Column(String(255), nullable=False)
    order = Column(Integer, nullable=False)

    site = relationship("Site", back_populates="pages")
    elements = relationship("Element", order_by=Element.id, back_populates="page")
