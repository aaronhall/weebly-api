from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from api import config
from Base import Base
from Page import Page


class Site(Base):
    __tablename__ = 'sites'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    title = Column(String(255), nullable=False)
    url = Column(String(255), nullable=False)
    is_published = Column(Boolean, default=False, nullable=False)

    user = relationship("User", back_populates="sites")
    pages = relationship("Page", order_by=Page.id, back_populates="site")

    def as_api_resource(self):
        return {
            '_link': '%s/sites/%s' % (config.API_PATH_PREFIX, self.id),
            'id': self.id,
            'title': self.title,
            'url': self.url,
            'is_published': self.is_published,
        }
