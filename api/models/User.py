from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from Base import Base
from api.models.Site import Site


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True, nullable=False)
    full_name = Column(String(255), nullable=False)
    signup_date = Column(DateTime, default=func.now())
    api_token = Column(String(255), unique=True, nullable=False)

    sites = relationship("Site", order_by=Site.id, back_populates="user")
