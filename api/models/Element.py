from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Text, UniqueConstraint
from sqlalchemy.orm import relationship
from Base import Base


class Element(Base):
    __tablename__ = 'elements'
    __table_args__ = (UniqueConstraint('page_id', 'order'),)

    id = Column(Integer, primary_key=True)
    page_id = Column(Integer, ForeignKey('pages.id'))

    content = Column(Text, nullable=True)
    order = Column(Integer, nullable=False)

    page = relationship("Page", back_populates="elements")
