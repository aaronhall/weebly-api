import falcon
from falcon import testing
import pytest

from api.app import create_app
from api.db import get_session







@pytest.fixture
def client():
    app = create_app(test_app=True)
    return testing.TestClient(app)


def test_get_sites_empty(client):
    expect = []
    response = client.simulate_get('/sites', headers={'Authorization': 'Token foo'})

    assert response.content == expect
    assert response.status == falcon.HTTP_OK
