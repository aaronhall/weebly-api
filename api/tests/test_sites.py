import falcon
from falcon import testing
import json
import logging
import unittest

from api import config
from api.app import create_app
from api.db import get_engine, get_session
from api.models import *


HEADERS = { 'Authorization': 'Token test1' }
CONN = get_engine('test')


class TestSites(unittest.TestCase):
    def setUp(self):
        Base.metadata.drop_all(CONN)
        Base.metadata.create_all(CONN)
        CONN.execute("INSERT INTO users VALUES (0, 'test1', 'test', NULL, 'test1')")
        CONN.execute("INSERT INTO users VALUES (0, 'test2', 'test', NULL, 'test2')")

        self.client = testing.TestClient(create_app('test'))

    def tearDown(self):
        CONN.dispose()
        Base.metadata.drop_all(CONN)

    def test_empty_sites(self):
        response = self.client.simulate_get('/sites', headers=HEADERS)
        self.assertEquals(json.loads(response.content), [])

        response = self.client.simulate_get('/sites/1', headers=HEADERS)
        self.assertEquals(response.status, falcon.HTTP_NOT_FOUND)

    def test_bad_permissions(self):
        # No token
        response = self.client.simulate_get('/sites')
        self.assertEquals(response.status, falcon.HTTP_UNAUTHORIZED)

        # Invalid token
        response = self.client.simulate_get('/sites', headers={ 'Authorization': 'Token bad_token'})
        self.assertEquals(response.status, falcon.HTTP_UNAUTHORIZED)

        # Test that user 2 doesn't have access to user 1's content
        test1_headers = HEADERS
        test2_headers = { 'Authorization': 'Token test2' }

        body = {
            'title': 'Sample title',
            'url': '/some/url'
        }
        response = self.client.simulate_post('/sites', body=json.dumps(body), headers=test1_headers)

        response = self.client.simulate_get('/sites', headers=test1_headers)
        self.assertEquals(len(json.loads(response.content)), 1)

        response = self.client.simulate_get('/sites', headers=test2_headers)
        self.assertEquals(len(json.loads(response.content)), 0)

        response = self.client.simulate_get('/sites/1', headers=test2_headers)
        self.assertEquals(response.status, falcon.HTTP_NOT_FOUND)

    def test_create_site(self):
        body = {
            'title': 'Sample title',
            'url': '/some/url'
        }
        expected = {
            '_link': '/api/v1/sites/1',
            'id': 1,
            'title': 'Sample title',
            'url': '/some/url',
            'is_published': False,
        }

        # Create new site
        response = self.client.simulate_post('/sites', body=json.dumps(body), headers=HEADERS)
        self.assertEquals(json.loads(response.content), expected)

        # New site is in sites list
        response = self.client.simulate_get('/sites', headers=HEADERS)
        self.assertEquals(json.loads(response.content), [expected])

        # Get by ID
        response = self.client.simulate_get('/sites/1', headers=HEADERS)
        self.assertEquals(json.loads(response.content), expected)

        # Creating a second site with the same URL should fail
        response = self.client.simulate_post('/sites', body=json.dumps(body), headers=HEADERS)
        self.assertEquals(response.status, falcon.HTTP_BAD_REQUEST)

        # Update title and is_published
        body = {
            'title': 'Another title',
            'is_published': True
        }
        expected['title'] = 'Another title'
        expected['is_published'] = True

        response = self.client.simulate_put('/sites/1', body=json.dumps(body), headers=HEADERS)
        self.assertEquals(json.loads(response.content), expected)

        # Get updated site by ID
        response = self.client.simulate_get('/sites/1', headers=HEADERS)
        self.assertEquals(json.loads(response.content), expected)

    def test_delete_site(self):
        self.fail('Not implemented')

    def test_create_site_missing_title(self):
        body = {
            'url': '/some/url'
        }
        response = self.client.simulate_post('/sites', body=json.dumps(body), headers=HEADERS)
        self.assertEquals(response.status, falcon.HTTP_BAD_REQUEST)

    def test_create_site_non_string_url(self):
        body = {
            'title': 'Title',
            'url': 1234
        }
        response = self.client.simulate_post('/sites', body=json.dumps(body), headers=HEADERS)
        self.assertEquals(response.status, falcon.HTTP_BAD_REQUEST)

    def test_create_site_extra_params(self):
        self.fail('Not implemented')

    def test_create_site_unicode_url_format(self):
        self.fail('Not implemented')

    def test_create_site_unicode_values(self):
        self.fail('Not implemented')
