import falcon
from jsonschema import validate
from jsonschema.exceptions import ValidationError
import json
import os
import yaml


RAML_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "raml/api.raml")
RAML = yaml.load(file(RAML_FILE))
RAML_SCHEMAS = { k: json.loads(v) for k, v in RAML['types'].iteritems() }

def validate_raml_type(type_key, value):
    try:
        validate(value, RAML_SCHEMAS[type_key])
    except ValidationError, e:
        raise falcon.HTTPBadRequest("Malformed request", e.message)

def filter_keys(dict, keys):
    keys = set(keys)
    return { k: v for k, v in dict.iteritems() if k in keys }
