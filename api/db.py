from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import config


ENGINES = {}

def get_engine(db_key='prod'):
    if db_key not in ENGINES:
        ENGINES[db_key] = create_engine(config.DATABASES[db_key], echo=False)

    return ENGINES[db_key]

def get_session(db_key='prod'):
    engine = get_engine(db_key)
    Session = sessionmaker(bind=engine)
    return Session()

def pass_db_session(f):
    """A decorator function to pass a DB session to a resource method as a keyword arg and
    guaranteeing that the session is closed, regardless of exceptions thrown in the inner function.
    """
    def wrapper(*args, **kwargs):
        self = args[0]
        db = self.get_db_session()
        kwargs['db'] = db

        try:
            return f(*args, **kwargs)
        except:
            raise
        finally:
            db.close()

    return wrapper
