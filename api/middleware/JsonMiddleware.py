import falcon
import json


class JsonMiddleware(object):
    """Transform request and response bodies to/from JSON."""

    def process_request(self, req, resp):
        """Process the request before routing it."""
        req.context['body'] = {}

        if req.content_length:
            try:
                doc = json.load(req.bounded_stream)
                req.context['body'] = doc
            except:
                raise falcon.HTTPBadRequest("Malformed JSON")


    def process_response(self, req, resp, resource, req_succeeded):
        """Post-processing of the response (after routing)."""

        # TODO: Falcon calls .encode on the response body before this gets called and fails,
        # expecting a string
        """
        if resp.body:
            try:
                encoded = json.dumps(resp.body)
                resp.body = encoded
            except:
                # TODO: do something
                pass
        """