
class CacheMiddleware(object):
    def process_request(self, req, resp):
        """Process the request before routing it."""
        pass

    def process_resource(self, req, resp, resource, params):
        """Process the request after routing."""
        pass

    def process_response(self, req, resp, resource, req_succeeded):
        """Post-processing of the response (after routing)."""