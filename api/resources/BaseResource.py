from sqlalchemy.orm import sessionmaker


class BaseResource(object):
    def __init__(self, engine):
        self.engine = engine

    def get_db_session(self):
        Session = sessionmaker(bind=self.engine)
        return Session()