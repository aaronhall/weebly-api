import falcon
import json

from api.db import pass_db_session
from api.models.Site import Site
from api.util import filter_keys, validate_raml_type
from BaseResource import BaseResource


class SiteCollection(BaseResource):
    @pass_db_session
    def on_get(self, req, resp, db):
        """GET /sites

        Get all sites owned by the authenticated user.

        Request body:
            None

        Response body:
            {
                sites: [
                    {
                        "_link": "/api/v1/sites/1",
                        "id": 1
                        "title": "Sample title",
                        "url": "/path/to/site",
                        "is_published": false
                    },
                    ...
                ]
            }
        """
        user = req.context['user']
        sites = db.query(Site).filter(Site.user_id == user.id)

        resp.body = json.dumps([site.as_api_resource() for site in sites])
        resp.status = falcon.HTTP_200

    @pass_db_session
    def on_post(self, req, resp, db):
        """POST /sites

        Create a new site for the authenticated user.

        Request body:
            {
                "title": "Sample title",
                "url": "/path/to/site",
                "is_published": false
            }

        Response body:
            {
                "_link": "/api/v1/sites/1",
                "id": 1
                "title": "Sample title",
                "url": "/path/to/site",
                "is_published": false
            }
        """
        user = req.context['user']
        params = req.context['body']

        validate_raml_type('site_new', params)

        # Check that the url is unique
        if db.query(Site).filter(Site.user_id == user.id, Site.url == params['url']).count():
            raise falcon.HTTPBadRequest("A site with this URL already exists")

        params = filter_keys(params, ('title', 'url', 'is_published'))
        site = Site(user_id=user.id, **params)
        db.add(site)
        db.commit()

        resp.body = json.dumps(site.as_api_resource())
        resp.status = falcon.HTTP_200


class SiteResource(BaseResource):
    @pass_db_session
    def on_get(self, req, resp, db, site_id):
        """GET /sites/<int:site_id>

        Get a site by the given ID.

        Request body:
            None

        Response body:
            {
                "_link": "/api/v1/sites/1",
                "id": 1
                "title": "Sample title",
                "url": "/path/to/site",
                "is_published": false
            }

        """
        user = req.context['user']
        site = db.query(Site).filter(Site.user_id == user.id, Site.id == site_id).first()

        if not site:
            raise falcon.HTTPNotFound()

        resp.body = json.dumps(site.as_api_resource())
        resp.status = falcon.HTTP_200

    @pass_db_session
    def on_put(self, req, resp, db, site_id):
        """PUT /sites/<int:site_id>

        Update a site by the given ID.

        Request body:
            {
                "title": "Sample title",
                "url": "/path/to/site",
                "is_published": false,
            }

        Response body:
            {
                "_link": "/api/v1/sites/1",
                "id": 1
                "title": "Sample title",
                "url": "/path/to/site",
                "is_published": false
            }

        """
        user = req.context['user']
        params = req.context['body']

        validate_raml_type('site_update', params)

        # Lookup site by ID
        site = db.query(Site).get(site_id)
        if not site:
            raise falcon.HTTPNotFound()

        # Check that the url is unique if provided
        if 'url' in params:
            if db.query(Site).filter(Site.user_id == user.id, Site.url == params['url']).count():
                raise falcon.HTTPBadRequest("A site with this URL already exists")

        # Filter request params and update the Site record
        params = filter_keys(params, ('title', 'url', 'is_published'))
        for k, v in params.iteritems():
            setattr(site, k, v)
        db.commit()

        resp.body = json.dumps(site.as_api_resource())
        resp.status = falcon.HTTP_200
