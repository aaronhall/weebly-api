from sqlalchemy import create_engine
from models import *
import config


engine = create_engine(config.DATABASE_URL, echo=True)

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)