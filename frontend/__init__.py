import os
import sys

# Make API's models importable from this package by including the parent directory in the python
# path.
sys.path.insert(1, os.path.join(sys.path[0], '..'))
