from .app import application as app
from api.db import get_session
from api.models.User import User

from flask import Flask, redirect, url_for, session, request, jsonify, render_template
from flask_oauthlib.client import OAuth


oauth = OAuth(app)
google = oauth.remote_app(
    'google',
    consumer_key='562905087128-a7gf0451phv6brri7a0eoqelsel2puau.apps.googleusercontent.com',
    consumer_secret='TqG3bp4soOVAJcNsRZF9lZgb',
    request_token_params={'scope': ['email']},
    base_url='https://www.googleapis.com/oauth2/v1/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://www.googleapis.com/oauth2/v4/token',
    authorize_url='https://accounts.google.com/o/oauth2/v2/auth',
)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/account')
def account():
    if not 'google_token' in session:
        return redirect(url_for('login'))

    user = google.get('userinfo').data
    if 'error' in user:
        return redirect(url_for('login'))

    db = get_session()
    user = db.query(User).filter_by(email=user['email']).first()
    if not user:
        return redirect(url_for('login'))

    return render_template('account.html', user=user)

@app.route('/login')
def login():
    return google.authorize(callback=url_for('authorized', _external=True))

@app.route('/logout')
def logout():
    session.pop('google_token', None)
    return redirect(url_for('index'))

@app.route('/login/authorized')
def authorized():
    resp = google.authorized_response()
    if resp is None or resp.get('access_token') is None:
        return 'Access denied: reason=%s error=%s resp=%s' % (
            request.args['error'],
            request.args['error_description'],
            resp
        )
    session['google_token'] = (resp['access_token'], '')

    guser = google.get('userinfo').data

    # TODO: generate auth token, make sure it's not already being used by another user
    db = get_session()
    user = db.query(User).filter_by(email=guser['email']).first()

    if not user:
        user = User(api_token='foo')

    user.email = guser['email']
    user.full_name = guser['name']
    db.add(user)
    db.commit()

    return redirect(url_for('account'))

@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')
